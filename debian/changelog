python-geoip2 (2.9.0+dfsg1-6) UNRELEASED; urgency=medium

  * Remove myself from Uploaders.

 -- Ondřej Nový <onovy@debian.org>  Mon, 18 Mar 2024 16:09:56 +0100

python-geoip2 (2.9.0+dfsg1-5) unstable; urgency=medium

  * Team upload.
  * d/control: add b-d on pytest to force tests to run via something other
    than the deprecated "python3 setup.py test" command.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Sat, 03 Dec 2022 20:40:20 -0500

python-geoip2 (2.9.0+dfsg1-4) UNRELEASED; urgency=medium

  * Update standards version to 4.6.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Mon, 14 Nov 2022 21:12:15 -0000

python-geoip2 (2.9.0+dfsg1-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

 -- Sandro Tosi <morph@debian.org>  Fri, 03 Jun 2022 11:59:19 -0400

python-geoip2 (2.9.0+dfsg1-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.

  [ Ondřej Kobližek ]
  * d/control:
    - Standards version is 4.4.0 now (no changes needed)
    - Bump compat level to 12
    - Remove unnecessary version constraints in depends
  * Drop Python 2 module
  * d/tests: Add Python module test
  * wrap-and-sort

 -- Ondřej Kobližek <koblizeko@gmail.com>  Thu, 25 Jul 2019 16:16:18 +0200

python-geoip2 (2.9.0+dfsg1-1) unstable; urgency=medium

  * New upstream release
  * d/control:
    - Remove ancient X-Python-Version field
    - Remove ancient X-Python3-Version field
    - Bump required version of python-maxminddb to 1.4
    - Standards version is 4.1.4 now (no changes needed)
  * Add upstream metadata
  * d/rules: Remove empty lines from the end of a file

 -- Ondřej Nový <onovy@debian.org>  Tue, 29 May 2018 13:47:31 +0200

python-geoip2 (2.8.0+dfsg1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Ondřej Kobližek ]
  * New upstream release
  * d/control: X-Python-Version raised up to 2.7

 -- Ondřej Kobližek <koblizeko@gmail.com>  Fri, 13 Apr 2018 08:11:46 +0200

python-geoip2 (2.7.0+dfsg1-1) unstable; urgency=medium

  * New upstream release
  * Update debhelper compat level to 11
  * d/control:
    - Standards version is 4.1.3 now (no changes needed)

 -- Ondřej Kobližek <koblizeko@gmail.com>  Fri, 02 Feb 2018 13:26:31 +0100

python-geoip2 (2.6.0+dfsg1-1) unstable; urgency=medium

  * New upstream release
  * d/copyright:
    - Add myself for Debian part
    - Https form of the copyright-format URL
  * d/control:
    - Added myself as uploader
    - Standards version is 4.1.1 now

 -- Ondřej Kobližek <koblizeko@gmail.com>  Thu, 02 Nov 2017 09:59:08 +0100

python-geoip2 (2.5.0+dfsg1-2) unstable; urgency=medium

  * Uploading to unstable

 -- Ondřej Nový <onovy@debian.org>  Tue, 20 Jun 2017 10:41:06 +0200

python-geoip2 (2.5.0+dfsg1-1) experimental; urgency=medium

  * New upstream release
  * d/copyright:
    - Add myself for Debian part
    - Bumped upstream copyright year

 -- Ondřej Nový <onovy@debian.org>  Fri, 12 May 2017 16:08:41 +0200

python-geoip2 (2.4.2+dfsg1-1) unstable; urgency=medium

  * New upstream release
  * Bumped required version of requests-mock to 0.5

 -- Ondřej Nový <onovy@debian.org>  Sun, 11 Dec 2016 15:49:57 +0100

python-geoip2 (2.4.1+dfsg1-1) unstable; urgency=medium

  * New upstream release.

 -- Martin Kratochvíl <martin.krata@gmail.com>  Mon, 05 Dec 2016 09:57:25 +0100

python-geoip2 (2.4.0+dfsg1-1) unstable; urgency=medium

  * Initial release (Closes: #844502).

 -- Martin Kratochvíl <martin.krata@gmail.com>  Mon, 14 Nov 2016 15:48:18 +0100
